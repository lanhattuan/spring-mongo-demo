package io.igate.springmongdemo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import io.igate.springmongdemo.document.Sector;
import io.igate.springmongdemo.dto.Sector.GetListSectorDTO;
import io.igate.springmongdemo.dto.Sector.ListSectorDTO;
import io.igate.springmongdemo.dto.Sector.SectorDTO;
import io.igate.springmongdemo.service.sector.SectorService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api")
public class SectorServiceController {
    @Autowired
    private SectorService sectorService;

    @PostMapping(value = "/sector")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Sector> createSector(@RequestBody SectorDTO sectorDTO) {
        return new ResponseEntity<>(sectorService.createSector(sectorDTO), HttpStatus.CREATED);
    }

    @GetMapping(value = "/sector/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Sector> getUser(@PathVariable(value = "id") String id){
        return new ResponseEntity<>(sectorService.getSector(id), HttpStatus.OK);
    }

    @GetMapping(value = "/sector")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ListSectorDTO> listSector(@RequestBody GetListSectorDTO getListSectorDTO) {
        int page = getListSectorDTO.getPage();
        int size = getListSectorDTO.getSize();
        String code = getListSectorDTO.getCode();
        Page<Sector> sector = sectorService.listSector(code, page, size);
        ModelMapper modelMapper = new ModelMapper();
        ListSectorDTO listSectorDTO = modelMapper.map(sector, ListSectorDTO.class);

        return new ResponseEntity<>(listSectorDTO, HttpStatus.OK);
    }

}
