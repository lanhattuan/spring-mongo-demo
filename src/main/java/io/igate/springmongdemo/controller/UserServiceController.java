package io.igate.springmongdemo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import io.igate.springmongdemo.document.User;
import io.igate.springmongdemo.dto.User.UserParamGetDTO;
import io.igate.springmongdemo.dto.User.UserUpdateDTO;
import io.igate.springmongdemo.dto.User.UsersListDTO;
import io.igate.springmongdemo.dto.User.UserDTO;
import io.igate.springmongdemo.service.user.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping(value = "/api")
public class UserServiceController {
    @Autowired
    private UserService userService;

    @GetMapping(value = "/user/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<User> getUser(@PathVariable(value = "id") String id){
        return new ResponseEntity<>(userService.getUser(id), HttpStatus.OK);
    }

//    @GetMapping(value = "/user")
//    @ResponseStatus(HttpStatus.OK)
//    public ResponseEntity<List> listUser(){
//        return new ResponseEntity<>(userService.listUsers(), HttpStatus.OK);
//    }
    @GetMapping(value = "/user")
    @ResponseStatus(HttpStatus.OK)
    //    public ResponseEntity<UsersListDTO> listSector(@RequestBody UserParamGetDTO userParamGetDTO) {
    public ResponseEntity<UsersListDTO> listSector(
        @RequestParam(value = "page", defaultValue = "0") int page,
        @RequestParam(value = "size", defaultValue = "10") int size,
        @RequestParam(value = "status", defaultValue = "") Integer status,
        @RequestParam(value = "keyword", defaultValue = "") String keyword
    ) {
//        int page = userParamGetDTO.getPage();
//        int size = userParamGetDTO.getSize();
//        String username = userParamGetDTO.getUsername();
        Page<User> users = userService.listUsers(keyword, status, page, size);
        ModelMapper modelMapper = new ModelMapper();
        UsersListDTO usersListDTO = modelMapper.map(users, UsersListDTO.class); //conversion from User entity to UsersListDTO:
        usersListDTO.setItems(users.getContent());
        usersListDTO.setTotalCount(users.getTotalElements());
        return new ResponseEntity<>(usersListDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/user")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<User> createUser(@RequestBody UserDTO userDTO){
        return new ResponseEntity<>(userService.createUser(userDTO), HttpStatus.CREATED);
    }

    @PutMapping(value = "/user/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<User> updateUser(@RequestBody UserUpdateDTO userUpdateDTO, @PathVariable(value = "id") String id){
        return new ResponseEntity<>(userService.updateUser(id,userUpdateDTO), HttpStatus.OK);
    }

    @DeleteMapping(value = "/user/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<User> deleteUser(@PathVariable(value="id") String id){
        return new ResponseEntity<>(userService.deleteUser(id), HttpStatus.OK);
    }

}
