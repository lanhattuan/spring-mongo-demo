package io.igate.springmongdemo.repository;


import io.igate.springmongdemo.document.Sector;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SectorRepository extends MongoRepository<Sector, String> {
//    @Query("{ 'code' : ?0 }") // lấy giống tuyệt đối
    @Query("{ 'code' : { $regex: ?0 } }") // lấy like
    Page<Sector> findByCodePage(String code, Pageable pageable);
}
