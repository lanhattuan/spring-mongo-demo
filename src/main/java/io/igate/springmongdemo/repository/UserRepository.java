package io.igate.springmongdemo.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import io.igate.springmongdemo.document.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
    //    @Query("{ 'code' : ?0 }") // lấy giống tuyệt đối
    @Query(value = "{$and: ["
                    + "{$or:[ {'username' : { $regex: :#{#keyword}, $options:'i' } }, {'email' : { $regex: :#{#keyword}, $options:'i' }} ]}"
                    + ",{$or:[ {'-1' : :#{#status}}, {'status' : :#{#status}} ]} ]}")
    Page<User> findByUsernamePage(@Param("keyword") String keyword, @Param("status") Integer status, Pageable pageable);
}
