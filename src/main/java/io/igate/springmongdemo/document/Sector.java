package io.igate.springmongdemo.document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection="sector")
public class Sector {
    @Id
    private String _id;

    private String code;
    @JsonProperty("name")
    private List<Translate> name;
    @JsonIgnore
    private Date createdDate;
    @JsonIgnore
    private Date updatedDate;

    public Sector(){
    }

    public Sector(String code, List<Translate> name, Date createdDate, Date updatedDate) {
        this._id = new ObjectId().toString();
        this.name = name;
        this.code = code;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }

    public String get_id() {
        return _id;
    }

    public String getCode() {
        return code;
    }

    public List<Translate> getName() {
        return name;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(List<Translate> name) {
        this.name = name;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    static public class Translate {
        @JsonProperty("languageId")
        private String languageId;

        @JsonProperty("Name")
        private String name;

        public String getLanguageId() {
            return this.languageId;
        }
        public void setLanguageId(String languageId) {
            this.languageId = languageId;
        }
        public String getName() {
            return this.name;
        }
        public void setName(String name) {
            this.name = name;
        }
    }
}
