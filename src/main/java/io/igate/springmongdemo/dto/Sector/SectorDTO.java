package io.igate.springmongdemo.dto.Sector;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.igate.springmongdemo.document.Sector;

import java.util.List;

public class SectorDTO {

    private String code;
    @JsonProperty("name")
    private List<Sector.Translate> name;

    public List<Sector.Translate> getName() {
        return name;
    }

    public String getCode() {
        return code;
    }


}
