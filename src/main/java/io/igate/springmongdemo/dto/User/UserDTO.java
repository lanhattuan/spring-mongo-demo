package io.igate.springmongdemo.dto.User;

public class UserDTO {

    private String name;
    private String username;
    private String password;
    private String dateOfBbirth;
    private Integer status;
    private String email;
    private String gender;

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getDateOfBbirth() {
        return dateOfBbirth;
    }

    public Integer getStatus() {
        return status;
    }

    public String getEmail() {
        return email;
    }

    public String getGender() {
        return gender;
    }
}
