package io.igate.springmongdemo.dto.User;

import io.igate.springmongdemo.document.User;

import java.util.List;

public class UsersListDTO {

    private int totalPages;
    private int totalElements;
    private long totalCount;
//    private int page;
    private int size;
    private int number;
//    private List<User> content;
    private List<User> items;

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(int totalElements) {
        this.totalElements = totalElements;
    }

//    public List<User> getContent() {
//        return content;
//    }
//
//    public void setContent(List<User> content) {
//        this.content = content;
//    }

//    public int getPage() {
//        return page;
//    }

//    public void setPage(int page) {
//        this.page = page;
//    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public List<User> getItems() {
        return items;
    }

    public void setItems(List<User> items) {
        this.items = items;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }
}
