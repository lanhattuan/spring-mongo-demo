package io.igate.springmongdemo.dto.User;

public class UserUpdateDTO {

    private String name;
//    private String password;
    private String dateOfBbirth;
    private Integer status;
    private String email;
    private String gender;

    public String getName() {
        return name;
    }

//    public String getPassword() {
//        return password;
//    }

    public String getDateOfBbirth() {
        return dateOfBbirth;
    }

    public Integer getStatus() {
        return status;
    }

    public String getEmail() {
        return email;
    }

    public String getGender() {
        return gender;
    }
}
