package io.igate.springmongdemo.service.sector;

import com.mongodb.MongoException;
import io.igate.springmongdemo.document.Sector;
import io.igate.springmongdemo.dto.Sector.SectorDTO;
import io.igate.springmongdemo.repository.SectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
public class SectorServiceImpl implements SectorService {
    @Autowired
    private SectorRepository sectorRepository;

    @Override
    public Sector createSector(SectorDTO sectorDTO) {
        Sector sector = new Sector();
        sector.setCode(sectorDTO.getCode());
        sector.setName(sectorDTO.getName());
        sector.setCreatedDate(new Date());
        sector.setUpdatedDate(new Date());
        return sectorRepository.save(sector);
    }

    @Override
    public Sector updateSector(String id, SectorDTO sectorDTO) {
        if (sectorRepository.findById(id).isPresent()){
            Sector existingSector = sectorRepository.findById(id).get();
            existingSector.setUpdatedDate(new Date());
            return sectorRepository.save(existingSector);
        }
        else
            throw new MongoException("Record not found");
    }

    @Override
    public Sector deleteSector(String id) {
        if (sectorRepository.findById(id).isPresent()){
            Sector sector = sectorRepository.findById(id).get();
            sectorRepository.delete(sector);
            return sector;
        }
        else
            throw new MongoException("Record not found");
    }

    @Override
    public Sector getSector(String id) {
        if (sectorRepository.findById(id).isPresent())
            return sectorRepository.findById(id).get();
        else
            throw new MongoException("Record not Found");
    }

    @Override
    public Page<Sector> listSector(String code,int page, int size) {
        Pageable paging = PageRequest.of(page, size);
        return sectorRepository.findByCodePage(code, paging);
    }
}
