package io.igate.springmongdemo.service.sector;

import io.igate.springmongdemo.document.Sector;
import io.igate.springmongdemo.dto.Sector.SectorDTO;
import org.springframework.data.domain.Page;

public interface SectorService {
    Sector createSector(SectorDTO sectorDTO);
    Sector updateSector(String id, SectorDTO sectorDTO);
    Sector deleteSector(String id);
    Sector getSector(String id);
    Page<Sector> listSector(String code, int page, int size);
}
