package io.igate.springmongdemo.service.user;

import java.util.Date;
import java.util.List;

import com.mongodb.MongoException;

import io.igate.springmongdemo.document.Sector;
import io.igate.springmongdemo.dto.User.UserDTO;
import io.igate.springmongdemo.dto.User.UserUpdateDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import io.igate.springmongdemo.document.User;
import io.igate.springmongdemo.repository.UserRepository;


@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;

    @Override
    public User createUser(UserDTO userDTO) {
        User user = new User();
        user.setName(userDTO.getName());
        user.setUsername(userDTO.getUsername());
        user.setDateOfBbirth(userDTO.getDateOfBbirth());
        user.setPassword(userDTO.getPassword());
        user.setEmail(userDTO.getEmail());
        user.setStatus(userDTO.getStatus());
        user.setGender(userDTO.getGender());
        user.setCreatedDate(new Date());
        user.setUpdatedDate(new Date());
        return userRepository.save(user);
    }

    @Override
    public User updateUser(String id, UserUpdateDTO userUpdateDTO) {
        if (userRepository.findById(id).isPresent()){
            User user = userRepository.findById(id).get();
            user.setName(userUpdateDTO.getName());
            user.setDateOfBbirth(userUpdateDTO.getDateOfBbirth());
//            user.setPassword(userUpdateDTO.getPassword());
            user.setEmail(userUpdateDTO.getEmail());
            user.setStatus(userUpdateDTO.getStatus());
            user.setGender(userUpdateDTO.getGender());
            user.setUpdatedDate(new Date());
            return userRepository.save(user);
        }
        else
            throw new MongoException("Record not found");
    }

    @Override
    public User deleteUser(String id) {
        if (userRepository.findById(id).isPresent()){
            User user = userRepository.findById(id).get();
            userRepository.delete(user);
            return user;
        }
        else
            throw new MongoException("Record not found");
    }

    @Override
    public User getUser(String id) {
        if (userRepository.findById(id).isPresent())
            return userRepository.findById(id).get();
        else
            throw new MongoException("Record not Found");
    }

    @Override
    public Page<User> listUsers(String keyword, Integer status, int page, int size) {
        Pageable paging = PageRequest.of(page, size);
        return userRepository.findByUsernamePage(keyword, status, paging);
    }
}
