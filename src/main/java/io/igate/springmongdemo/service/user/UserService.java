package io.igate.springmongdemo.service.user;

import io.igate.springmongdemo.dto.User.UserDTO;
import io.igate.springmongdemo.document.User;
import io.igate.springmongdemo.dto.User.UserUpdateDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserService {
    User createUser(UserDTO userDTO);
    User updateUser(String id, UserUpdateDTO userUpdateDTO);
    User deleteUser(String id);
    User getUser(String id);
    Page<User> listUsers(String keyword, Integer status, int page, int size);
}
